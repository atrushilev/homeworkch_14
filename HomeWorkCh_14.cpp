﻿// HomeWorkCh_14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

int main()
{
    cout << "Enter your word: ";
    string s;
    cin >> s; 
    cout << "Your word is:  " << s << endl;
    cout << "The length of the word is: " << s.length() << endl;
    cout << "The first letter is: " << s[0] << endl;
    cout << "The last letter is: " << s.back() << endl;
}
